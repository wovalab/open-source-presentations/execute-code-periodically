:experimental:

== Execute Code Periodically

There are several methods to schedule code execution at regular intervals in a LabVIEW program.
On desktop operating systems, I find it helpful to create a "Helper loop" using a while loop and an event structure.
In this 15-minute presentation, I will demonstrate the correct way to implement this method to ensure a consistent execution period.

The presentation has been created for GLA Summit 2024 https://www.glasummit.org/presentations/88b7ce1a-9be5-4da2-a0f8-52ee1f2a17c2[here].


== Display the presentation

Open the `presentation.html` file in your favorite browser

[TIP]
====
.Usefull keybord shortcut
* kbd:[<-] kbd:[->] kbd:[↑] kbd:[↓] to navigate into the presentation
* kbd:[O] to open slide navigator
* kbd:[F] to switch to full-screen mode
* kbd:[S] to open the speaker view
====

== How to build the presentation

Presentation is made with https://docs.asciidoctor.org/reveal.js-converter/latest/[Asciidoctor Reveal.js]

IMPORTANT: To have the following instructions working, you must install Ruby and Bundler as described in the https://docs.asciidoctor.org/reveal.js-converter/latest/setup/ruby-setup/#prerequisites[Prerequisites section] of the https://docs.asciidoctor.org/reveal.js-converter/latest/[Asciidoctor Reveal.js] documentation

. Move to the repository root folder in a terminal
. Execute the following command `bundle exec asciidoctor-revealjs presentation.adoc`
