== Data Visualization

image::Data_Visualization_icon.png[]

image::Data_Visualization.png[]

// Disable previous image attribute to fix bug in amges inserted <- to be investigated
:!imgpath:

.Functions description
[cols="<.^1a,^.^3a,<.^3a", %autowidth, frame=all, grid=all, stripes=none]
|===
|Name |Connector pane |Description

|Create
a|image::Wovalab_lib_AsciiDoctor.lvlib_Line_Chart.lvclass_Create.vi.png[]
|Create a diagram bloc representing a ine chart.

|Create
a|image::Wovalab_lib_AsciiDoctor.lvlib_Grouped_Bar_Chart.lvclass_Create.vi.png[]
|Create a diagram bloc representing a line chart.

* Keys are the different axes' names.
* Categories are the set of values' names.
* Data is a 2D array where lines are the categories' values set for each key (columns).

|Create
a|image::Wovalab_lib_AsciiDoctor.lvlib_Radar_Chart.lvclass_Create.vi.png[]
|Create a diagram bloc representing a line chart.

* Keys are the different axes' names.
* Categories are the set of values' names.
* Data is a 2D array where lines are the categories' values set for each key (columns).

|Create Diagram
a|image::Wovalab_lib_AsciiDoctor.lvlib_Diagram.lvclass_Create_Diagram.vi.png[]
|Create a diagram bloc.

Diagrams are described using plain text. The format of the text description depends on the selected diagram type.

More information: https://docs.asciidoctor.org/diagram-extension/latest/#creating-a-diagram
|===
