== Attributes

image::Attributes_icon.png[]

image::Attributes.png[]

// Disable previous image attribute to fix bug in amges inserted <- to be investigated
:!imgpath:

.Functions description
[cols="<.^1a,^.^3a,<.^3a", %autowidth, frame=all, grid=all, stripes=none]
|===
|Name |Connector pane |Description

|Set Image Directory
a|image::Wovalab_lib_AsciiDoctor.lvlib_Doc.lvclass_Set_Image_Directory.vi.png[]
|Sets the directory for locating images added to the document.

|Set Table Of Content
a|image::Wovalab_lib_AsciiDoctor.lvlib_Doc.lvclass_Set_Table_Of_Content.vi.png[]
|Enable or disable the creation of a table of contents in the generated document

|Set Table Of Content Levels
a|image::Wovalab_lib_AsciiDoctor.lvlib_Doc.lvclass_Set_Table_Of_Content_Levels.vi.png[]
|Set the number of section levels (i.e. subsection depth) shown in the table of contents

|Set Section Numbering
a|image::Wovalab_lib_AsciiDoctor.lvlib_Doc.lvclass_Set_Section_Numbering.vi.png[]
|Enable or disable section numbering in the generated document

|Add Custom Attribute
a|image::Wovalab_lib_AsciiDoctor.lvlib_Doc.lvclass_Add_Custom_Attribute.vi.png[]
|Add a custom attribute to the document.

Attributes can be references in the text using {"attribute name"}. Allowing e.g. text subsitutions etc.

For more information see: https://dosc.asciidoctor.org/asciidoc/latest/attributes/document-attributes
|===
