[background-image="intro.png",background-size=cover,background-opacity="1"]
== Who Am I?

* Olivier Jourdan
* 20+ years programing in {lv}
* Founder of Wovalab in 2018
* Board member of the DQMH Consortium

icon:linkedin-square[2x] https://www.linkedin.com/in/jourdanolivier/

[.notes]
--
Independent Consultant
I'm mainly helping people to design and develop {lv} applications. Still, I'm also helping with the entire development process from Git usage to CI/CD process and as you may notice if you already know me, I try to help with documentation.

For those who were in Amsterdam last year, I hope you remember I talked about creating documents, test reports in LabVIEW.
One of the last slides of my presentation introduced the {dc} approach.

I'm glad to have the opportunity today to talk about it.

And, I promise you this will be my last presentation about documentation. I'll not continue to bother you with this topic. 

--